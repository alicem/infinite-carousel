namespace Infinitepaginator {
	[GtkTemplate (ui = "/org/gnome/InfinitePaginator/window.ui")]
	public class Window : Hdy.ApplicationWindow {
		public const int N_PAGES = 2;

		[GtkChild]
		private Hdy.Carousel carousel;
		private int position;

		public Window (Gtk.Application app) {
			Object (application: app);
		}

		construct {
			position = 0;

			for (int i = 0; i <= N_PAGES; i++)
				carousel.add (create_page (i));

			for (int i = -1; i >= -N_PAGES; i--)
				carousel.prepend (create_page (i));
		}

		[GtkCallback]
		private void page_changed_cb (Hdy.Carousel carousel, uint index) {
			position += ((int) index - N_PAGES);

			for (; index < N_PAGES; index++) {
				carousel.remove (carousel.get_children ().last ().data);
				carousel.prepend (create_page (position - (int) index - 1));
			}

			for (; index > N_PAGES; index--) {
				carousel.remove (carousel.get_children ().first ().data);
				carousel.add (create_page (position - (int) index + 2 * N_PAGES + 1));
			}
		}

		private Gtk.Widget create_page (int i) {
			var label = new Gtk.Label ("Page %d".printf (i));
			label.expand = true;
			label.show ();

			return label;
		}
	}
}
